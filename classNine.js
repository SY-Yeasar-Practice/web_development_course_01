// var a = 5
// a++ 
// var b = a++ + ++a 
// var c  = a 
// --a 
// console.log(a)
// console.log(b)
// console.log(c)
// output: 7, 14, 8

// var a = 5
// a++ //6
// a = a++ + ++a 
// var b = a; 
// --a 
// console.log(a)
// console.log(b)
//output: 13, 14

// var a = 5;
// a++ 
// a = a++
// var b = a; 
// --a 
// console.log(a) 
// console.log(b)

// // output : 5,6

// var a = 5; 
// a -- //4
// a = a++ + 5 //9
// var b = a;//9
// a-= 5 + a
// console.log(a)
// console.log(b)

//comparison operator 
// return a boolean  
//true or false 
// == 
// console.log(5 == 5) // true or false

// 1 . == (equality)
// 2. === (equality)
// 3. > (greater than )
// 4. >= (greater than and equal)
// 5. < (less than)
// 6 . <= (less than and equal)
// 7. != (not equal)
// 8. !== (not double equal)

// == (check only the value ignore the datatype)
// console.log(5 == "5")
// === (check value and datatype both)
// console.log(5 === "5")

// console.log("10" > 10)
// console.log("15" >= 15)
// console.log("10" < 15)
// console.log("15" <= 15)


// not equal !=  (check the value only ignore the datatype)
// console.log(5 != 5);
// console.log(5 != "5");

//not double equal !== (check the value or the datatype)
// console.log(5 !== 5)

// console.log(5 !== 6)
// console.log("6" !== 6)

// console.log ("true" == 1);

// if datatype is same then just compare the value 
// console.log("5" == "5")
// console.log(5 == 5)

// if datatype are different 
// compare between string and number
// convert the string to number
// then compare the both number
// console.log("15" == 15)

// compare between string and boolean 
//convert both datatype to number first then compare both numbers
// console.log(true == "1") // 1 == 1 = true

//falsy value  (6 types)
//1. 0
//2. ""
//3. null 
//4. NaN 
//5. undefined
//6. false

// console.log(undefined == undefined)
// console.log(undefined == NaN )
// console.log(NaN >= NaN)

// NaN is always be false 

// console.log(undefined == undefined)
// console.log(undefined == null)

// console.log("undefined" == undefined) (NaN == NaN)
// console.log(+undefined)
// console.log("undefined" == undefined) 

//work with null 
// console.log(null == "0") 
// console.log(null >= "0")
// console.log("null" == 0); // NaN == 0 // false
// console.log(null == 0) //false
// console.log(null == undefined)


// console.log(50) //decimal
// console.log (050)  //octal
// console.log (0x50) //hexadecimal
// 050 => octal 
// 40 => decimal
// 50 => decimal
// console.log("050" == 50) //40 == 50
// console.log(0500 == 320 )


//logical operator 
    // AND && 
    // OR || 
    // NOT !
//return => boolean 

//AND 
// var a = null && 100 //short circuit  AND operator // find alway falsy  and if not found then execute the last expression
// console.log(a)
//OR
// var a = null || undefined;  //find always truthy value and if not found then execute the last expression
// console.log(a)

// console.log(5 || false || 1)

// console.log(!!"") //false => true => false
// console.log(!!"Hello")

// console.log(!!"hello") //any datatype including boolean to boolean
// console.log (Boolean(!false))
//


// console.log (5 || 15)

//??
// console.log (undefined ?? 15)
//left hand side => null or undefined => execute RHS
// var a;
// console.log (a ?? 5)

// console.log(2 ** 2)  

//type of 
// console.log(typeof typeof 5)
// console.log(5 > 15 > 10)

// //normal then catch
// const studentName = () => {
//     return new Promise (resolve => {
//         resolve(`Hello I am shopnil`)
//     })
// }
// const studentAge = (data) => {
//      return new Promise (resolve => {
//         console.log({data})
//         resolve(`${data} I am 25 years age `)
//     })
// }
// const studentClass = (data) => {
//     return new Promise (resolve => {
//         resolve(`${data} I am in Class two`)
//     })
// }

// studentName()
// .then (res=> {
//     return studentAge(res)
// })
// .then((res)=> {
//     return studentClass(res)
// })
// .then (res => {
//     console.log(res)
// }) 
// .catch (err => {
//     console.log(err.message)
// })

// //with async await 
// async function main () {
//    try {
//         const studentName = () => {
//             return new Promise (resolve => {
//                 resolve(`Hello I am shopnil`)
//             })
//         }
//         const studentAge = () => {
//             return new Promise (resolve => {
//                 resolve(`I am 25 years age `)
//             })
//         }
//         const studentClass = () => {
//             return new Promise (resolve => {
//                 resolve(`I am in Class two`)
//             })
//         }
//         const name = await studentName ()
//         const age = await studentAge ()
//         const classes = await studentClass ()
//         console.log(`${name} ${age} ${classes}`)
//    }catch (err) {
//     console.log(err.message)
//    }
// }
// main()

//operator precedence 

// 5 + 8 * 5 + 5 //Bodmas

// console.log (5 * 5 + 10 / 10)

// 5 + ( (5 / 5) * 5)



//sf;ks;kf;sdlakf;ls

