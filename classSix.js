// var a = 5; 
// var b = new Number (5);
// console.log (a == b)
// console.log (typeof a + " " + typeof b)
// var a = 5 

// number 
// new Number (5)


// var a = new String (55) //object form
// var b = "55" //literal form 

//string 
//new String (55)
// var a  = "55"

//boolean 
// new Boolean()

//symbol 
// new Symbol()


// var a = new Number (0);
// var b = Number (0)


//operator 

// 55 + 55 //55 is operand and + is operator

// 5 types of operator 
// 1. Arithmetic operator (+,-,*,/,%,++,--) 7 operators
// 2. Assignment Operator (=, +=, -=, *=, %=, /=) 6 operators
// 3. Logical Operator (&&, ||, !)
// 4. Comparison operator (<,>,<=, >=, ==, ===, !=, !== ) 
// 5. Conditional Operator or ternary operator (?...:)

//Extra operator 
// 1. Nullish 
// 2 . (...)(spread operator)
// 3 (...) (rest operator)
// 4. (**)
// var a = 3 ** 3;
// console.log(a)
// 5. Typeof operator

let i
for (let i = 0; i < 5; i++) {
    function hello () {
        console.log(i)
    }
    setTimeout(hello, 200)
}
console.log(i)