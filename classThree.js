// "use strict"
// var name = "Tonmoy";
// "" '' ``
// var message = "My name is ";


//var let const

// var name; //variable declaration
// var age = 5; //variable declaration with value assign
// name = "shopnil";

// var varsity = 'IUB';
//reserve word (can not use it as a variable name)

// var name = "shopnil";
//$ _ 

//space  (not allowed)
// var first name = "shopnil"

//case sensitive

// var age = 25;
// Age = 55;
// console.log(Age)

// var firstName = "shopnil";


// var firstName  = "shopnil";
// firstName = "newaz"

// var varsity;

// varsity = "IUB";
// console.log(varsity)
// varsity = "NSU"
// console.log(varsity)

var person = "shopnil";
person = "tonmoy";
person = person;
console.log(person)
